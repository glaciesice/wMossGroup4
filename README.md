# wMoss
### Software Engineering Project Management (ISYS 1106)

---

## Main Objective of the Project
The objective of the project is to replace current Cinema Aurora's booking system with an online booking system. The purpose is to improve the customer experience instead of using Console Based search, it will be using the modern web technologies. The system will enable all of Cinema Aurora's customers to access currently playing movies in real time and book immediately from the website with user friendly interface.

## Project Specification
The scope of the project is to make web technologies that enable Cinema Aurora to have its own website and online booking system for their customers and settle the payment online from the web itself. To see the complete assignment from the course, refer to the documents inside `Assignment Specification` folder.

## Technologies used and Prerequisites
These are the technologies that we are using to develop wMoss system. Some software are required to be installed first before compiling and deploying the system.

### Frameworks and Libraries
>* [Laravel](https://laravel.com/)
* [Bootstrap](https://getbootstrap.com/)

### Setups
Before deploying the system, there are some prerequisites before installing the frameworks and libraries itself. Below are only the steps of setting up the development environment for our system:

#### Backend Environment (Laravel)
Before running the backend source code, it is required to install `Laravel`'s dependencies first by using [`Composer`](https://getcomposer.org/). Follow the website's instruction to install `Composer` into your computer. After you have successfuly install `Composer`, simply go to `./backend` folder and run this command

>`composer install`

This will install the Laravel's dependencies. After the dependencies installation success, create `.env` file based on `.env.exaple`. `.env` file will be described later. After copying the template, run:

>`./artisan key:generate`

This will generate the application key and the development environment is now available for run. Simply use this command to start the development server:

>`./artisan serve`

The system does not use any Database System because of the specification says. The system manages its own pseudo-database on a specific folder specified on `.env` file. Make sure you edit the `PSEUDO_DB_PATH_FOLDER` to the correct folder. If no path is not specified, that would cause a problem to our system... But if you specify a blank folder (or a folder that has a content, which is a bad idea) or even folder that does not exists, it will manages itself to create that folder and the pseudo-database files. A sample of `.env` file are available on `.env.example`.

## Contributors
* Andre Valentino (s3702774)
* Laurensius Frederik Tanno (s3702759)
* Marvin Tanudjaja (s3702777)
* Thomas Dwi Dinata (s3702763)

## Acknowledgments
We would like to thank to everyone/someone who inspires us:

<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use App\Http\Controllers\AuthenticationCentre;
use Illuminate\Http\Response;
use App\Http\Controllers\DatabaseManager;

class UserIdentification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $response = $next($request);

      if(!AuthenticationCentre::verifyCookie($request)){
        $response->withCookie('customerCookieId', AuthenticationCentre::generateCookie(), 2880);
      }

      return $response;
    }

    private function generateCookie($request, Closure $next){
      $newData = AuthenticationCentre::makeNewCookie();
      $token = $newData["customerCookieId"];
      DatabaseManager::insert("customers",$newData);
      return $token;
    }
}

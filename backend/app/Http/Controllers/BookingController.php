<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Inacho\CreditCard;

class BookingController extends Controller
{
	private $theatre="theatre";
	private $booking="customers";
  private $seats="seats";

    public static function generateBookingId(){
  	$id='';
  	$keys=range('A','Z');
  	for($i=0;$i<3;$i++){
  		$id.=$keys[array_rand($keys)];
  	}
  	$key_num='';
  	$key_range=range(1,9);
  	for($i=0;$i<5;$i++){
  		$key_num.=$key_range[array_rand($key_range)];
  	}
  	$bookingId=$id.$key_num;
  	return $bookingId;
  }

  public function finalisePayment(Request $req){
  	$findcookie=DatabaseManager::findExactly($this->booking,["customerCookieId"=>$req->cookie('customerCookieId')]);//find customer's coookie ID
  	if(empty($findcookie)){
  		abort(400,'user invalid');
  	}
  	$email="";
  	$creditcard="";
  	$fullname="";
  	$cvv="";
  	$expiration="";
  	if(empty($findcookie)){//check if cookie is valid or not
  		abort(404,'invalid cookie');
  	}
  	else{
  		if(isset($req->email))
  			$email=$req->email;
  		if(isset($req->creditcard)){
				if(!CreditCard::validCreditCard($req->creditcard)["valid"]){
					$c = new CustomerController();
					return $c->getCheckout($req);
				}
				$creditcard=$req->creditcard;
			}
  		if(isset($req->fullname))
  			$fullname=$req->fullname;
  		if(isset($req->cvv)){
  			$cvv=$req->cvv;
			}
  		if(isset($req->expiration))
  			$expiration=$req->expiration;

      	$cart=$findcookie[0]->shoppingCart;//get customer's shopping cart
        $seats=[];
        $i=0;
        foreach($cart as $index){
          array_push($seats, $index->seats);//fetch a list of seats from the shopping cart
          $i++;
        }
        $i=0;
        $seatList=[];
        foreach($seats as $seat){
            array_push($seatList, $seat);
          $i++;
        }
        $i=0;
        $playtimeId=[];
        foreach($cart as $index){
          array_push($playtimeId, $index->playtimeMovieId);//fetch a list of playtime IDs
          $i++;
        }
      $i=0;
      $date=[];
      foreach($cart as $index){
          array_push($date, $index->date);//fetch a list of playtime IDs
          $i++;
        }

      $a = [];
      $a['playtimeMovieId'] = $playtimeId[0];
      $a['date'] = $date[0];
      $a['seats'] = $seatList[0];

  		try{
  			DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["customerName"=>$fullname]);/* Fill the storage with user details*/
	  		DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["ccNumber"=>$creditcard]);
	  		DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["expiry"=>$expiration]);
	  		DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["email"=>$email]);
        DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["progress"=>"PAID"]);
        DatabaseManager::insert($this->seats,$a);
        $bookingId=$this->generateBookingId();
        DatabaseManager::update("customers",["customerCookieId"=>$req->cookie('customerCookieId')],["bookingId"=>$bookingId]);
  		}
  		catch(Exception $e){
        print_r("error");
  			return back()->with('error',$e);
  		}
  		return $this->confirmPayment($req);
  	}
  }

  public function emptyCart(Request $req){
    $findcookie=DatabaseManager::findExactly($this->booking,["customerCookieId"=>$req->cookie('customerCookieId')]);//find customer's coookie ID
    if(empty($findcookie)){
      abort(400,'user invalid');
    }
    else{
      DatabaseManager::update($this->booking,["customerCookieId"=>$req->cookie('customerCookieId')],["shoppingCart"=>[]]);

    }
  }

  public function confirmPayment(Request $rq){
    $cookie = $rq->cookie("customerCookieId");
    //$customer=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')]);
    $customer=DatabaseManager::findExactly($this->booking,["customerCookieId"=>$rq->cookie('customerCookieId')])[0];
    //verify customer cookie
      if(empty($customer)){
        abort(404,'cart not found');
      }
      else{
        $cart=$customer->shoppingCart;//get customer's shopping cart
        $movieIDs=[];
        $i=0;
        foreach($cart as $index){
          array_push($movieIDs, $index->movieId);//fetch a list of movie IDs from the shopping cart
          $i++;
        }
        $i=0;
        for($i=0; $i<count($customer->shoppingCart); $i++) {
          $movieName=DatabaseManager::findExactly("movies",["movieId"=>$movieIDs[$i]])[0];//fetch the movie name
          $customer->shoppingCart[$i]->movieId=$movieName->movieTitle;
        }
        $bookingId=$customer->bookingId;
				DatabaseManager::update("customers",["customerCookieId"=>$rq->cookie('customerCookieId')],["shoppingCart"=>[]]);
        return view('orderConfirmation')->with('cart',$customer)->with('bookingId',$bookingId);
  }
}

	public function seats($movieId, $date, $time){
		$seats = $this->isSeatAvailable($time, $date, $movieId);
		$movieTitle = DatabaseManager::findExactly("movies", ["movieId"=>$movieId])[0]->movieTitle;
		$dateString = date("j F Y", strtotime($date));
		return view('seatSelection')->with('seats', $seats)->with('movieId', $movieId)->with('date', $date)->with('time', $time)->with('movieTitle', $movieTitle)->with("dateString", $dateString);
	}

	public function isSeatAvailable(string $time, string $date, string $movieId){
		$data = DatabaseManager::findExactly("movies", ["movieId"=>$movieId])[0]->moviePlayTime;//gets an array of movieplaytime
		$id = '';
		$booked=[];
		foreach($data as $datum){
			if($datum->time == $time){
				$id = $datum->playtimeMovieId;//gets the playtimemovieId
				break;
			}
		}
		if(empty($id)){
			abort(404,'page not found');
		}
		else{
			$theatre = DatabaseManager::findMatch("seats", ["playtimeMovieId"=>$id]);//find if seats are occupied based on playtimemovieid
			if(empty($theatre)){
				return $booked;
			}
			foreach($theatre as $t){
				foreach($t->seats as $seat)
					array_push($booked, $seat);
			}
		}
		return $booked;
	}

	public function seatsUpdate(Request $rq, $movieId, $date, $time){
		$customerSeat = DatabaseManager::findExactly("customers", ["customerCookieId"=>$rq->cookie('customerCookieId')])[0]->shoppingCart;
		foreach($customerSeat as $seat){
			if($seat->movieId == $movieId){
				$customerSeat = $seat->seats;
				break;
			}
		}
		$otherSeats = $this->isSeatAvailable($time, $date, $movieId);
		$movieTitle = DatabaseManager::findExactly("movies", ["movieId"=>$movieId])[0]->movieTitle;
		$dateString = date("j F Y", strtotime($date));
		return view('editSeat')->with('otherSeats', $otherSeats)->with('customerSeats', $customerSeat)->with('movieId', $movieId)->with('time', $time)->with('date',$date)->with('movieTitle', $movieTitle)->with('dateString', $dateString);
	}

}

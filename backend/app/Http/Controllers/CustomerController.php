<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{

  private $customers="customers";
  private $movie="movies";

  public function __construct(){
    date_default_timezone_set("Australia/Melbourne");

   }
  public function getShoppingCart(Request $rq){


  	// $cookie = $request->cookie("customerCookieId");
  	$cookie = $rq->cookie("customerCookieId");
    //$customer=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')]);
    $customer=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$rq->cookie('customerCookieId')])[0];
    //verify customer cookie
      if(empty($customer)){
        abort(404,'cart not found');
      }
      else{
      	$cart=$customer->shoppingCart;//get customer's shopping cart
      	$movieIDs=[];
      	$i=0;
      	foreach($cart as $index){
      		array_push($movieIDs, $index->movieId);//fetch a list of movie IDs from the shopping cart
      		$i++;
      	}
      	$i=0;
      	for($i=0; $i<count($customer->shoppingCart); $i++) {
      		$movieName=DatabaseManager::findExactly($this->movie,["movieId"=>$movieIDs[$i]])[0];//fetch the movie name
      		$customer->shoppingCart[$i]->movieId=$movieName->movieTitle;
          //$customer->shoppingCart[$i]->
      	}
      	return view('shoppingCart')->with('cart',$customer);
      }
  }

  public function getCheckout(Request $rq){


  	// $cookie = $request->cookie("customerCookieId");
  	$cookie = $rq->cookie("customerCookieId");
    //$customer=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')]);
    $customer=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$rq->cookie('customerCookieId')])[0];
    //verify customer cookie
      if(empty($customer)){
        abort(404,'cart not found');
      }
      else{
      	$cart=$customer->shoppingCart;//get customer's shopping cart
        if(count($cart) < 1)
          return $this->getShoppingCart($rq);
      	$movieIDs=[];
      	$i=0;
      	foreach($cart as $index){
      		array_push($movieIDs, $index->movieId);//fetch a list of movie IDs from the shopping cart
      		$i++;
      	}
      	$i=0;
      	for($i=0; $i<count($customer->shoppingCart); $i++) {
      		$movieName=DatabaseManager::findExactly($this->movie,["movieId"=>$movieIDs[$i]])[0];//fetch the movie name
      		$customer->shoppingCart[$i]->movieId=$movieName->movieTitle;
      	}
      	return view('checkout')->with('cart',$customer);
      }
  }

  public function addToCart(Request $request){

  	$flag=0;
  	$playingId;
    $price = 0;
  	$findcookie=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')]);//find customer's coookie ID
  	if(empty($findcookie)){
  		abort(400,'user invalid');
  	}
  	else{
  		$movies=DatabaseManager::findExactly($this->movie,["movieId"=>$request->movieId]);//verify that the movie exists
	  	if(empty($movies)){
	  		abort(404,'movie not found');
	  	}
	  	else{
	  		$checktime=$movies[0]->moviePlayTime;//fetch movie playtimes
        $price = $movies[0]->price;
	  		$playingId=$this->getPlayingId($checktime,$request->time);//fetch playtime ID from the movie table
	  		if(empty($playingId)){
	  			abort(400,'invalid time provided');
	  		}
	  	/*
	  		get a list of seats by getting the request key value
	  	*/
	  	$keys=array_keys($request->seats);
	  	$seats=[];
	  	foreach($keys as $seat){
	  		array_push($seats, $seat);
	  	}
      $shoppingCartArray = DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')])[0]->shoppingCart;
      $shoppingCart=[];//shopping cart array, collect all items into shopping cart then push into database
      $shoppingCart['movieId'] = $request->movieId;
      $shoppingCart['seats'] = $seats;
      $shoppingCart['price'] = $price;
      $shoppingCart['playtimeMovieId'] = $playingId;
      $shoppingCart['date']=$request->date;
      array_push($shoppingCartArray, $shoppingCart);
	  		/*
	  			populate the cart with cookieId, and shoppingCart array
	  		*/
	  		try{
	  			DatabaseManager::update($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')],["shoppingCart"=>$shoppingCartArray]);
	  		}
	  		catch(Exception $e){
	  			return redirect('cart')->with('error', $e);
	  		}

	  	}
	  	return redirect('cart')->with('status','Item added to cart');
  	}
  }

  public function updateSeats(Request $rq){
		$movieId = $rq->movieId;
		$time = $rq->time;
		$date = $rq->date;
		$keys=array_keys($rq->seats);
		$seats=[];
		foreach($keys as $seat){
			array_push($seats, $seat);
		}
		$iterator = 0;
		$currentSeat = DatabaseManager::findExactly("customers", ["customerCookieId"=>$rq->cookie('customerCookieId')])[0]->shoppingCart;
		foreach($currentSeat as $seat){
			if($seat->movieId == $movieId){
				$currentSeat[$iterator]->seats = $seats;
				break;
			}
			$iterator++;
		}
		DatabaseManager::update("customers", ["customerCookieId"=>$rq->cookie('customerCookieId')], ["shoppingCart"=>$currentSeat]);

    return redirect('cart')->with('status','New seat selection has been saved');
	}

  public function getPlayingId($checktime,$time){
  	$playingId='';
  	foreach($checktime as $showtime){//iterate through the moviePlayTime array
  			if($showtime->time==$time){//check if the provided time is valid
  				$playingId=$showtime->playtimeMovieId;

  				break;
  			}
  		}
  	return $playingId;
  }

  public function deleteItem(Request $request){
  	$findcookie=DatabaseManager::findExactly($this->customers,["customerCookieId"=>$request->cookie('customerCookieId')]);//find customer's coookie ID
  	if(empty($findcookie)){
  		abort(400,'user invalid');
  	}
  	else{

  		try{
  			$cart=DatabaseManager::findExactly($this->customer,["customerCookieId"=>$request->cookie('customerCookieId')])[0]->shoppingCart;//fetch cart
  			$i=0;
  			foreach($cart as $movie){
  				if($movie->movieId==$request->movieId){
  					break;
  				}
  				$i++;
  			}
  			DatabaseManager::update($this->customer,["customerCookieId"=>$request->cookie('customerCookieId')],array_splice($cart,$i, 1));
  		}
  		catch(Exception $e){
  			return back()->with('error',$e);
  		}
  	}
  	return back()->with('success','item deleted from cart');
  }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthenticationCentre extends Controller
{
    public static function makeNewCookie(){
      $customerCookieId = AuthenticationCentre::makeToken();
      $cookieExpiry = strtotime('+2 day',time());
      $shoppingCart = [];
      $customerName = "";
      $paymentProgress = [];
      $bookingId = "";
      $data = [
        "customerCookieId"=>$customerCookieId,
        "cookieExpiry"=>$cookieExpiry,
        "shoppingCart"=>$shoppingCart,
        "customerName"=>$customerName,
        "paymentProgress"=>$paymentProgress,
        "bookingId"=>$bookingId
      ];
      return $data;
    }

    public static function extendTime(string $cookie){
      $date = DatabaseManager::findExactly("customers",["customerCookieId"=>$cookie])[0]->cookieExpiry;
      if(DatabaseManager::update("customers", ["customerCookieId"=>$cookie],["cookieExpiry"=>strtotime('+2 days', $date)]));
    }

    private static function makeToken()
    {
      $token = '';
      if(function_exists('openssl_random_pseudo_bytes'))
        $token = bin2hex(openssl_random_pseudo_bytes(64));
      else if(function_exists('random_bytes'))
        $token = bin2hex(random_bytes(64));
      return $token;
    }

    public static function verifyCookie(Request $rq){
      $isOk = false;
      if($rq->cookie('customerCookieId') == null) {
        $isOk = false;
      } else {
        if(DatabaseManager::findExactly("customers", ["customerCookieId" => $rq->cookie('customerCookieId')]) != []){
          if(DatabaseManager::findExactly("customers", ["customerCookieId"=>$rq->cookie('customerCookieId')])[0]->cookieExpiry > time()){ // If the expiry is still long...
            AuthenticationCentre::extendTime(\Cookie::get('customerCookieId'));
            $isOk = true;
          } else // Make new token if old cookie already expired
            $isOk = false;
        }
      }
      return $isOk;
    }

    public static function generateCookie(){
      $newData = AuthenticationCentre::makeNewCookie();
      $token = $newData["customerCookieId"];
      DatabaseManager::insert("customers",$newData);
      return $token;
    }

    public static function getCookie(Request $rq){
      return $rq->cookie('customerCookieId');
    }
}

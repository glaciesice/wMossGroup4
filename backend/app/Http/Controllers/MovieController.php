<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use \nazbav\JsonDb\JsonDb;

class MovieController extends Controller
{

	//public $directory="/databases/"
   private $file="movies";
   private $mov="movieTitle";

   public function __construct(){
    date_default_timezone_set("Australia/Melbourne");
   }

    public static function allMovies()
    {
      $movies=DatabaseManager::all("movies");//fetch all movies
      //print_r($movies);
      return ($movies);//returns a list of movie names
    }

    public static function playingNowName($movies){
        //retrieve key attributes of the
      $i=0;
        $playingNow=[];
        $playingNowList=[];
        $playingParam=["moviePlayingNow"=>1];
        $playingKeys=array_keys($playingParam);
        foreach ($movies as $datum) {
          if($datum->{$playingKeys[0]} == $playingParam[ $playingKeys[0] ]){
            array_push($playingNow, $movies[$i]);//gets a list of playing now movies
          }
        ++$i;
        }
      $i=0;
      // dd($playingNow);
      // foreach($playingNow as $movieName){
      //
      //     $movieName=$playingNow[$i]->movieTitle;//gets a movie name
      //
      //     array_push($playingNowList, $movieName);
      //     ++$i;
      //   }
        return $playingNow;
    }

    public static function comingSoonName($movies){
      $comingSoonParam=["moviePlayingNow"=>0];
      $comingSoonList=[];
      $comingSoon=[];
      $comingSoonKeys=array_keys($comingSoonParam);
        $i=0;
        foreach ($movies as $datum) {
          if($datum->{$comingSoonKeys[0]} == $comingSoonParam[ $comingSoonKeys[0] ]){
            array_push($comingSoon, $movies[$i]);
          }
          ++$i;
        }

          $i=0;

        // foreach($comingSoon as $movieName){
        //
        //     $movieName=$comingSoon[$i]->movieTitle;//gets a movie name
        //
        //     array_push($comingSoonList, $movieName);
        //     ++$i;
        //   }
          return $comingSoon;
      }

    public static function getDesc($list,$param){
        $i=0;
        $playingNow=[];
        $desclist=[];
        $playingParam=$param;
        $playingKeys=array_keys($playingParam);
        foreach ($list as $datum) {
          if($datum->{$playingKeys[0]} == $playingParam[ $playingKeys[0] ]){
            array_push($playingNow, $list[$i]);//gets a list of currently airing movies
          }
        ++$i;
        }
      $i=0;
      foreach($playingNow as $movieName){

          $movieName=$playingNow[$i]->movieDescription;//gets a movie name

          array_push($desclist, $movieName);
          ++$i;
        }
      return $desclist;
    }

    public function playingNow($id){
       //$dbm=new DatabaseManager();
      //$db=$dbm->getDb();
	  //$table="movies.json";
      $i=0;
      $movieNameList=[];
      $movies=DatabaseManager::findMatch($this->file,["moviePlayingNow"=>1]);
      if(empty($movies)){
        abort(404,'there are no currently airing movies');
      }
      else{
        $toBeReturned = [];
      $itemPerPage = 4;
      $page = $id;

      for($i = ($itemPerPage * ($page-1)); ($i < (($itemPerPage * ($page-1))+$itemPerPage)) && ($i < count($movies));++$i)
          array_push($toBeReturned, $movies[$i]);
      /*foreach($movies as $movieName){

        $movieName=$movies[$i]->{$this->mov};//gets a movie name

        array_push($movieNameList, $movieName);
        ++$i;
      }*/
      return view('playingNow')->with('movies',$toBeReturned);
      }

    }

    public function comingSoon($id){
      $i=0;
      $movieNameList=[];
       $movies=DatabaseManager::findMatch($this->file,["moviePlayingNow"=>0]);
       if(empty($movies)){
        abort(404,'no coming soon movies found');
      }
      else{
        $toBeReturned = [];
       $itemPerPage = 4;
       $page = $id;
       for($i = ($itemPerPage * ($page-1)); ($i < (($itemPerPage * ($page-1))+$itemPerPage)) && ($i < count($movies));++$i)
           array_push($toBeReturned, $movies[$i]);
        return view('comingSoon')->with('movies',$toBeReturned);
      }
    }

    public function getMovie($id,$date){
      $movie=DatabaseManager::findExactly($this->file,["movieId"=>$id]);
      if(empty($movie)){
        abort(404,'movie not found');
      }
      else{
        $timeList=[];

        $playingTime=$movie[0]->moviePlayTime;//get the moviePlayTime entry

        //get ticket price
        $price=DatabaseManager::all("price");
        return view('movieDescription')->with('movie',$movie)->with('price',$price);
    }

  }

    public function getTime($id){
      $movie=DatabaseManager::findExactly($this->file,["movieId"=>$id]);//get a movie entry
      if(empty($movie)){
        abort(404,'movie not found');
      }
      else{
        $timeList=[];

        $playingTime=$movie[0]->moviePlayTime;//get the moviePlayTime entry

        $i=0;
        foreach($playingTime as $time){//try to fetch the 'time' entry from moviePlayTime array
            $seconds=$time->time;//convert to seconds
            array_push($timeList, date("H:i:s", $seconds));
            $i++;
        }
        return $timeList;
      }

    }

    public function getByName(Request $rq){
      // dd($rq->name);
      $movie=DatabaseManager::findMatch($this->file,["movieTitle"=>$rq->name]);
      if(empty($movie)){
        error(404, "no movie found");
      }
      return view('search')->with('movie',$movie);
    }
}

<?php

namespace App\Http\Controllers;

use App\Exceptions\PseudoDbException;

class DatabaseManager
{
  /*
    NEW DATABASE SYSTEM BY SOMEONE

    How to use:
    The instance are static, so you can immediately use this:
    DatabaseManager::all("movies"); // Return ALL data inside that Table
    DatabaseManager::findMatch("movies", ["movieId"=>"MV0008"]); // FInd matching criteria

    How to do RAW things:
    The data returned by DatabaseManager::all() are all decoded JSON, which is an array that looks like the JSON.
    To access it, you can just call like an array. Suppose the code is like this:

    $a = DatabaseManager::all("movies");

    And assume $a got data as below:
    [
      {
        "movieId":"MV0001",
        "movieName":"Avengers Infinity War"
      },
      {
        "movieId":"MV0002",
        "movieName":"Lorem Ipsum"
      },
      {
        "movieId":"MV0003",
        "movieName":"FooBar"
      }
    ]

    To access MV0002, you can just call $a[1]. But to access the data like the movieId content,
    You can call like this:
    $a[1]->movieId;

    But what if you have a variable that contains the name of the data, you can use it too!
    $z = "movieId";
    $a[1]->{$z}; // This equals to $a[1]->movieId, The {} bracket used to return the content of $z and assign it

  */
  private static $dbPath = "";
  private static $dbPtr = "";

  private static function openTable(string $tableName){
    DatabaseManager::$dbPath = getenv("PSEUDO_DB_PATH");
    if(DatabaseManager::$dbPath == "" || empty(DatabaseManager::$dbPath) || DatabaseManager::$dbPath == null)
      throw new PseudoDbException("No file name was set on the Environment Variable! Please set is first in .env file! Please set it with key `PSEUDO_DB_PATH`. Don't forget to add '/' at the end!");

    // Just for checking...
    if(!is_writeable(DatabaseManager::$dbPath))
      throw new PseudoDbException("Server does not able to read/write on specified folder! Double check whether PHP has access to it!");

    try {
      DatabaseManager::$dbPtr = fopen(DatabaseManager::$dbPath . DatabaseManager::addExtension($tableName), "r+");
    } catch (\Exception $e) { // If read is not possible, then create it!
      DatabaseManager::$dbPtr = fopen(DatabaseManager::$dbPath . DatabaseManager::addExtension($tableName), "w+");
    }

    fseek(DatabaseManager::$dbPtr,0);
  }

  private static function addExtension(string $tableName) : string {
    return $tableName . ".json";
  }

  private static function close(){
    fclose(DatabaseManager::$dbPtr);
    $dbPtr = null;
  }

  public static function getDbPath() : string{
    return DatabaseManager::$dbPath;
  }

  public static function all(string $tableName){
    $buffer = "";
    $data = "";
    DatabaseManager::openTable($tableName);
    while(($buffer = fgets(DatabaseManager::$dbPtr, 4096)) !== false)
      $data .= $buffer;
    if(empty($data)) // If no data present (E.g. New File)
      $data = json_decode("[]"); // Initialise it with empty data
    else
      $data = json_decode($data);
    DatabaseManager::close();
    return $data;
  }

  public static function page(string $tableName, $page, $itemPerPage){
    $data = DatabaseManager::all($tableName);
    $toBeReturned = [];
    for($i = ($itemPerPage * ($page-1)); ($i < (($itemPerPage * ($page-1))+$itemPerPage)) && ($i < count($data));++$i)
        array_push($toBeReturned, $data[$i]);
    return $toBeReturned;
  }

  public static function findExactly(string $tableName, $parameter){ // Returns ONLLY ONE data
    $i = 0;
    $data = DatabaseManager::all($tableName);
    if(empty($data)){
      return [];
    }
    $akeys = array_keys($parameter);
    foreach ($data as $datum) {
      if($datum->{$akeys[0]} == $parameter[ $akeys[0] ]){
        return [$data[$i]];
      }
      $i++;
    }
    return [];
  }

  public static function findMatch(string $tableName, $parameter){
    $i = 0;
    $data = DatabaseManager::all($tableName);
    $akeys = array_keys($parameter);
    $dataList=[];
    foreach ($data as $datum) {
      if($datum->{$akeys[0]} == $parameter[ $akeys[0] ]){
        array_push($dataList, $data[$i]);
      }
      $i++;
    }

    return $dataList;
  }

  public static function insert(string $tableName, $newData){ // Data MUST be in array!!!
    $data = DatabaseManager::all($tableName);
    if(empty($data))
      $data = [];
    array_push($data, $newData);
    $json = json_encode($data);
    DatabaseManager::$dbPtr = fopen(DatabaseManager::$dbPath . DatabaseManager::addExtension($tableName), "w+"); // Overwrite all data in the file
    fwrite(DatabaseManager::$dbPtr, $json);
  }

  public static function update(string $tableName, $parameter, $update){
    $valid = false;
    $i = 0;
    $data = DatabaseManager::all($tableName);
    $akeys = array_keys($parameter);
    $ukeys = array_keys($update);
    for($i = 0; $i < sizeof($data);++$i){
      if($data[$i]->{$akeys[0]} == $parameter[ $akeys[0] ]){
        $data[$i]->{$ukeys[0]} = $update[ $ukeys[0] ];
        $valid = true;
        break;
      }
    }
    if($valid){
      DatabaseManager::$dbPtr = fopen(DatabaseManager::$dbPath . DatabaseManager::addExtension($tableName), "w+"); // Overwrite all data in the file
      fwrite(DatabaseManager::$dbPtr, json_encode($data));
    }
    return $valid;
  }

  public static function delete(string $tableName, $parameter){
    $data = DatabaseManager::all($tableName);
    $isValid = false;
    $akeys = array_keys($parameter);
    $i = 0;
    foreach($data as $datum){
      if($datum->{$akeys[0]} == $parameter[ $akeys[0] ]){
        array_splice($data, $i, 1);
        $isValid = true;
        break;
      }
      ++$i;
    }
    if($isValid){
      DatabaseManager::$dbPtr = fopen(DatabaseManager::$dbPath . DatabaseManager::addExtension($tableName), "w+"); // Overwrite all data in the file
      fwrite(DatabaseManager::$dbPtr, json_encode($data));
    }
  }

}

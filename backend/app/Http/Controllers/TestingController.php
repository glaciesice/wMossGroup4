<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\DatabaseManager;
use App\Http\Controllers\AuthenticationCentre;
use Inacho\CreditCard; // See https://github.com/inacho/php-credit-card-validator

class TestingController extends Controller
{
    public function index(Request $rq){
      $i=0;
      $id=1;
      $movieNameList=[];
      $movies=DatabaseManager::findMatch("movies",["moviePlayingNow"=>1]);
      $toBeReturned = [];
      $itemPerPage = 4;
      $page = $id;
      for($i = ($itemPerPage * ($page-1)); ($i < (($itemPerPage * ($page-1))+$itemPerPage)) && ($i < count($movies));++$i)
          array_push($toBeReturned, $movies[$i]);

      return view('playingNow')->with('movies',$toBeReturned);
    }

    public function ccValidity(string $string){
      return CreditCard::validCreditCard($string)["valid"];
    }

    public function setCookie(Request $request){
      $minutes = 1;
      $response = new Response('Hello World');
      $response->withCookie(cookie('name', 'virat', $minutes));
      return $response;
   }

   public function getCookie(Request $request){
      $value = $request->cookie('name');
      echo $value;
   }
}

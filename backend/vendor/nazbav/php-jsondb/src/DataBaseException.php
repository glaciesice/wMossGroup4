<?php
/**
 * File: Exception.php;
 * Author: nazbav;
 * Date: 19.04.2018;
 * Time: 22:23;
 */

namespace nazbav\JsonDb;


/**
 * Class Exception
 *
 * @package nazbav\JsonDb
 */

class DataBaseException extends \Exception
{

}
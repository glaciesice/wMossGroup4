<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Input;



Route::group(['middleware' => 'userIdentification'], function () {
	Route::get('/',function(){
		return view('home');
	});
	Route::get('/test', 'TestingController@index');
	Route::get('/PlayingNow/id={id}','MovieController@playingNow');

	Route::get('/ComingSoon/id={id}','MovieController@comingSoon');

	Route::get('/AboutUs', function(){
		return view('aboutUs');
	});

	Route::get('/MovieDescription/{id}/{date}', 'MovieController@getMovie');

	Route::post('/search/', 'MovieController@getByName');

	Route::get('/faq', function(){
		return view('FAQ');
	});

	Route::get('/cart','CustomerController@getShoppingCart');
	
	Route::get('/Booking/id={movieId}/date={date}/time={time}', 'BookingController@seats');

	Route::get('/checkout','CustomerController@getCheckout');

	Route::post('/checkout','BookingController@finalisePayment');

	Route::post('/editSeat', 'CustomerController@updateSeats');

	Route::get('/editSeat/id={movieId}/date={date}/time={time}', 'BookingController@seatsUpdate');

	Route::get('/confirmation','BookingController@confirmPayment');

	Route::post('/seat/select', 'CustomerController@addToCart');

	Route::post('/cart','CustomerController@deleteItem');


});

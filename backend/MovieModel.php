<?php

$db = new \Filebase\Database(['dir'=>'/','format'=>\Filebase\Format\Json::class,'cache'=>true,'cache_expires'=>1800,'pretty'=>true,'safe_filename'=>true,'read_only'=>false,]);

// How to get data and choose a table, actually, this is the way to get instance of \Filebase\Document...
$item = $db->get("movie"); // This will get the file "movie.json" and the table movie. If not exist, it will create a new one, just like MongoDB

// To modify a data, simply use this
$item->name = "Avengers";
$item->id = "MV001";
$item->description = "Lorem ipsum dolor sit amet";

// To save the data, use this, ALSO UPDATE
$item->save();

// To find a data, use this
$d = $db->query()->where("column_name","SQL_COMPARISON","Value_to_be_searched")->results();

// To delete a data
$item->delete();

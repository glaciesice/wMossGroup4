
  @include('header')
      <?php
        $url = "/checkout";
      ?>

        <!-- End of Navbar -->

        <div class="row progressBarParent bs-wizard" style="border-bottom:0;">

            <div class="progressBar col-xs-4 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum"><h5>Shopping Cart</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum"><h5>Payment</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step active"><!-- active -->
              <div class="text-center bs-wizard-stepnum"><h5>Order Confirmation</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>
        </div>

        <center class="bookingConfirmation">
          <h1>Thank You</h1>
          <h5>Your Booking ID is</h5>
          <h4>{{$bookingId}}</h5>
        </center>
        <?php $amount=0?>
       <table class="shoppingCartTable">
          <tr>
            <th>Title</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Sub-Total</th>
          </tr>
          @for($i=0; $i<count($cart->shoppingCart); $i++)
          <tr>
            <td class="cartContent">{{$cart->shoppingCart[$i]->movieId}}</td>
            <td class="cartContent">{{count($cart->shoppingCart[$i]->seats)}}</td>
            <td class="cartContent">{{$cart->shoppingCart[$i]->price}}</td>
            <td class="cartContent">{{ $amount+=((int)$cart->shoppingCart[$i]->price * count($cart->shoppingCart[$i]->seats)) }}</td>
          </tr>
          @endfor
        </table>

        <table class="totalCartTable">
          <tr>
            <td class="totalContent"></td>
            <td class="TotalContent">Total</td>
            <td class="TotalContent">{{$amount}}</td>
          </tr>
        </table>

        @include('footer')


  @include('header')

        <!-- End of Navbar -->

        <div class="searchPage">
          <h1 class="title-page">Searching for Thor...</h1>
          <hr style="width:90%">

          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <form class="form-inline">
                <input class="mr-sm-1" type="search" placeholder="Search" aria-label="Search">
                <button class="btn my-2 my-sm-0 searchButton" type="submit"><i class="fas fa-search"></i></button>
              </form>
            </div>
          </div>

          <div class="container">
            <div class="col-md-12">
              <div class="jumbotron" style="background-color: white">
                <div class="container">

                  <div class="row">
                    <div class="col-md-1">

                    </div>
                    @foreach($movie as $mv)
                      <div class="col-md-4" href="#">
                        <div class="card">
                          <img class="card-img-top" src="{{ asset('image/nowPlaying/'.$movie[0]->movieTitle.'.jpg') }}" alt=thor.jpg>
                            <div class="card-body">
                              <h4 class="card-title">{{$movie[0]->movieTitle}}</h4>
                              <p class="card-text">
                                {{$movie[0]->movieDescription}}
                              </p>
                              <a href="#!" class="btn btn-primary">Read More</a>
                          </div>
                          </div>
                      </div>
                      <div class="col-md-2"></div>
                    @endforeach
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('footer')

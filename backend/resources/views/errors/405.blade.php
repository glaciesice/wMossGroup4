<?php $i=0?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cinema Aurora</title>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
        <!-- Fonts -->
    </head>
    <body>

      <!-- Navbar -->
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="/"><img class="d-block w-100" src="{{ asset('image/cinemaAuroraLogo.png') }}" alt="Cinema Aurora"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background-color: black" >
            <span class="navbar-toggler-icon" style="background-color: white"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent" >

            <ul class="nav navbar-nav ml-auto" style="margin-right: 3em;">
              <li class="nav-item search-container">
                <form class="form-inline my-2 my-lg-0" action="search/" method="post" >
                  {{ csrf_field() }}
                  <input class="form-control mr-sm-1" name="name" type="search" placeholder="Search" aria-label="Search">
                  <button class="btn my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                </form>
              </li>
              <li class="nav-item active navbarCustom">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item navbarCustom">
                <a class="nav-link" href="/PlayingNow/id=1">Playing Now</a>
              </li>
              <li class="nav-item navbarCustom">
                <a class="nav-link" href="/ComingSoon/1">Coming Soon</a>
              </li>
              <li class="nav-item navbarCustom">
                <a class="nav-link" href="#">F A Q</a>
              </li>
              <li class="nav-item navbarCustom">
                <a href="/cart">
                    <img src="{{ asset('image/shoppingCart.png') }}"/>
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <!-- End of Navbar -->
        <div class="container">
          <div class="col-md-12">
            <div class="jumbotron" style="background-color: white">
              <center><h1>405 Method Not Allowed</h1></center>
            </div>
          </div>
        </div>




        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </body>
    <!--Footer-->
    <footer class="page-footer font-small blue pt-4 mt-4">
        <div class="container-fluid text-center text-md-left">
            <hr style="width: 90%" />
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <ul class="list-unstyled">
                        <li>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3 otherLinks">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="/AboutUs">
                                                  <h4>About us</h4>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                  <h4>Term of Use</h4>
                                              </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                  <h4>Job Opportunity</h4>
                                              </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm copyright">
                                        THX and Lucasfilm are &#0169; Lucasfilm Ltd. TM all rights are reserved. Used under authorization. &#0169; 2000 Sony Cinema Products Corporation. All Rights Reserved. Sony, Sony Dynamic Digital Sound and SDDS are trademarks of Sony Corporation.
                                        &#0169; 1999-2018 CinemaAurora.com. All materials and context (texts, graphics, and every attributes) of CinemaAurora.com website are copyrights and trademarks of Cinema Aurora
                                        Any Commercia usage of materials and contents is forbidden without prior permission from CinemaAurora. There is no other instituions/agencies outside CinemaAurora allowed to use www.cinemaAurora.com (Cinema Aurora Website) without prior permission from Cinema Aurora
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--/.Second column-->
            </div>
        </div>
        <!--/.Footer Links-->
    </footer>
    <!--/.Footer-->
</html>

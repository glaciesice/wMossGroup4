
  @include('header')

        <!-- End of Navbar -->

        <center><h2>Frequently Asked Question</h2></center>

        <div class="container ">
          <div class="panel-group" id="faqAccordion">
            <div class="panel panel-default QnApanel">

              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                <h4 class="panel-title">
                  <a href="#question0" class="ing">How do I search for movies?</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>You can simply type your preferences of movies at the top of the menu, next to the title of the website.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                <h4 class="panel-title">
                  <a href="#question1" class="ing">How do I seek a specific movie detail?</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>You can simply select a movie and press either Read More or Order Ticket inorder to view the movie details.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                <h4 class="panel-title">
                  <a href="#question2" class="ing">How do I book tickets in this website?</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>In order to book a ticket, you have to select a movie, by going to the Now Playing page, and select <b>Order Ticket</b>.
                    Once you have accessed the movie details page, scroll down to the bottom of the page and select the schedule according to your preference of date and time.
                    You will be re-directed to a page where you can select your seats based on your order. Once you are done, you can either finish your transaction or continue shopping.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                <h4 class="panel-title">
                  <a href="#question3" class="ing">I want to book a ticket, but the button only displays Read More.</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>If the button only mentions Read More, it means that the current movie details is just a teaser and is not available to be purchased at the current moment.
                    It will re-display as 'Order Ticket' once the movie is available to be purchased.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
                <h4 class="panel-title">
                  <a href="#question4" class="ing">I want to book a ticket, but I cannot click the button.</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>Your selected preference has been fully booked, and therefore it may not be able to be purchased.
                    We recommend you to choose the next available schedule.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question5">
                <h4 class="panel-title">
                  <a href="#question5" class="ing">What type of credit cards are accepted?</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>All major credit cards including <b>Mastercard</b> and <b>Visa</b> are accepted.
                  </p>
                </div>
              </div>
            </div>

            <div class="panel panel-default QnApanel">
              <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question6">
                <h4 class="panel-title">
                  <a href="#question6" class="ing">Where can I contact the cinema?</a>
                  <i class="fas fa-angle-down"></i>
                </h4>
              </div>
              <div id="question6" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                  <hr>
                  <p>You can access our contact page by accessing our <a href="/AboutUs">about us</a> page which can be found at the bottom of the page.
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>

        @include('footer')


  @include('header')<?php $k=0?>
      <?php
        $date = Route::input('date');
      ?>

        <!-- End of Navbar -->
        <div class="container movieDescription">
          <div class="col-md-12">
            <div class="jumbotron" style="background-color: white">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                  <img src="{{ asset('image/nowPlaying/'.$movie[$k]->movieTitle.'.jpg') }}" width="100%" height="100%">
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-5">
                    <h1>{{$movie[$k]->movieTitle}}</h1>
                    <hr>
                    <h5>Category : {{$movie[$k]->movieGenre[0]}}, {{$movie[$k]->movieGenre[1]}}</h5>
                    <h5>Director : {{$movie[$k]->movieDirector}}</h5>
                    <h5>Duration : {{((int)($movie[$k]->movieDuration))/60}} mins</h5>
                    <h5>Casting</h5>
                    @for($h = 0; $h< 4;$h++)
                    <div class="cast">
                      <div class="card">
                        <img class="card-img-top" src="{{ asset('image/casts/'.$movie[$k]->movieTitle.'/'.$movie[$k]->movieCast[$h].'.jpg') }}" alt="{{ $movie[$k]->movieCast[$h] }}">
                        <div class="card-body">
                          <p class="card-text">{{$movie[$k]->movieCast[$h]}}</p>
                        </div>
                      </div>
                    </div>
                    @endfor
                    <h5 class="synopsis">Synopsis</h5 >
                    <h5 class="synopsisDescription">{{$movie[$k]->movieDescription}}</h5>

                    <h3 class="price">Price</h3 >
                    <h4 style="font-weight: bold; color: black;" class="synopsisDescription">{{$movie[$k]->price}}</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <center>
            <h2>Trailer</h2>
            <div class="trailer">
              <iframe width="90%" height="100%" src="https://www.youtube.com/embed/{{$movie[$k]->movieTrailer}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </center>
          <div class="jumbotron" style="background-color: white;"></div>
          <?php
            if($movie[$k]->moviePlayingNow == 1){
          ?>
            <div class="jumbotron" style="background-color: white; margin-top: -5%">
              <hr style="color: black;">
              <div class="container">
                <div class="row">
                  <div class="col-md-2">
                    <h2 style="color: black">SCHEDULES</h2>
                  </div>
                  <div class="col-md-2"></div>
                  <div class="col-md-7">
                    <div class="container">
                      <div class="row">
                        <?php

                          $datetime = new DateTime;
                          for($i = 0 ; $i< 7; $i++){
                            ?>
                            <div class="col-md-3">
                              <?php
                                $url = "/MovieDescription/".$movie[$k]->movieId."/".$datetime->format("d-m-Y");
                              ?>
                              <a href="{{url($url)}}" style="text-decoration: none"><h3 style="display: inline;">{{$datetime->format("d-m")}}</h3></a>
                            </div>
                            <?php
                            $datetime->modify("+1 day");
                          }

                        ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <?php
                $theater = 0;
                $start = 0;

                for($i = 0; $i < count($movie[$k]->moviePlayTime); $i++){
                  if($movie[$k]->moviePlayTime[$i]->theatre != $theater){
                    $theater = $theater + 1;
                    if($start != 0){
                    ?>
                      </div>
                        </div>
                    <?php
                    }
                    $start = 1;

                ?>
                    <div class="container" style="margin-top: 5%">
                      <div class="row">
                      <div class="col-md-2">
                        <h3>Theatre {{$movie[$k]->moviePlayTime[$i]->theatre}}</h3>
                      </div>

                <?php
                  }
                  $url = "/Booking/id=".$movie[$k]->movieId."/date=".$date."/time=".$movie[$k]->moviePlayTime[$i]->time;
                ?>
                    <div class="col-md-3" >
                      <?php
                        if($movie[$k]->moviePlayTime[$i]->availableSeats != 0){
                      ?>
                        <center><button class="btn btn-outline-secondary custom" onclick="window.location='{{ url("$url") }}'">{{$movie[$k]->moviePlayTime[$i]->time}}</button></center>
                      <?php
                        }
                        else{
                      ?>
                        <center><button class="btn btn-outline-secondary custom" disabled onclick="window.location='{{ url("$url") }}'" style="background-color: red">{{$movie[$k]->moviePlayTime[$i]->time}}</button></center>
                        <p style="color:red;font-size: 1em">Sorry, The seats are full, please choose another showing time.</p>
                      <?php
                        }
                      ?>
                    </div>
              <?php
                }
              ?>
            </div>
          </div>
        <?php
          }
        ?>
      @include('footer')

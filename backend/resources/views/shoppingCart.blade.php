
  @include('header')
        <script>
        $(function() {
          $(".expand").on( "click", function() {
            // $(this).next().slideToggle(200);
            $expand = $(this).find(">:first-child");

            if($expand.text() == "+") {
              $expand.text("-");
            } else {
              $expand.text("+");
            }
          });
        });
        </script>
        <?php
        $url = "/checkout";
      ?>
        <!-- End of Navbar -->

        <div class="row progressBarParent bs-wizard" style="border-bottom:0;">

            <div class="progressBar col-xs-4 bs-wizard-step active">
              <div class="text-center bs-wizard-stepnum"><h5>Shopping Cart</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum"><h5>Payment</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum"><h5>Order Confirmation</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>
        </div>
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif

      @if (session('error'))
          <div class="alert alert-danger">
              {{ session('error') }}
          </div>
      @endif

        <table class="shoppingCartTable">
          <tr>
            <th>Title</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Sub-Total</th>
            <th>Action</th>
          </tr>
          <?php $amount = 0; ?>
          @for($i=0; $i<count($cart->shoppingCart); $i++)
          <tr>
            <td class="cartContent">{{$cart->shoppingCart[$i]->movieId}}</td>
            <td class="cartContent">{{count($cart->shoppingCart[$i]->seats)}}</td>
            <td class="cartContent">{{$cart->shoppingCart[$i]->price}}</td>
            <td class="cartContent">{{ (int)$cart->shoppingCart[$i]->price * count($cart->shoppingCart[$i]->seats) }}</td>
<?php $amount += (int)$cart->shoppingCart[$i]->price * count($cart->shoppingCart[$i]->seats) ?>
            <td class="cartAction"><a href="#">edit</a></td>
            <form method="post" action ="/cart">
              <td class="cartDelete"><input type="submit" class="btn-outline-secondary" name="Delete" value="Delete"></td>  
            </form>
            
          </tr>
          @endfor
        </table>

        <table class="totalCartTable">
          <tr>
            <td class="totalContent"></td>
            <td class="totalContent"></td>
            <td class="TotalContent">Total</td>
            <td class="TotalContent">{{$amount}}</td>
            <td colspan="2" class="totalContent"></td>
          </tr>
        </table>

        <div class="escapeCart">
          <button type="button" class="btn btn-outline-secondary" onclick="window.location='/PlayingNow/id=1'">Continue Shopping</button>
          <button type="button" class="btn btn-primary" onclick="window.location='{{ url("$url") }}'">Checkout</button>
        </div>

        @include('footer')

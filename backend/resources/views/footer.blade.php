
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/popper.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
<!--Footer-->
<footer class="page-footer font-small blue pt-4 mt-4">
  <div class="container-fluid text-center text-md-left">
    <hr style="width: 90%" />
    <div class="row">
      <div class="col-md-2">
      </div>
      <div class="col-md-8">
        <ul class="list-unstyled">
          <li>
            <div class="container">
              <div class="row">
                <div class="col-sm-3 otherLinks">
                  <ul class="list-unstyled">
                    <li>
                      <a href="/AboutUs">
                        <h4>About us</h4>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <h4>Term of Use</h4>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <h4>Job Opportunity</h4>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-sm copyright">
                  THX and Lucasfilm are &#0169; Lucasfilm Ltd. TM all rights are reserved. Used under authorization. &#0169; 2000 Sony Cinema Products Corporation. All Rights Reserved. Sony, Sony Dynamic Digital Sound and SDDS are trademarks of Sony Corporation. &#0169;
                  1999-2018 CinemaAurora.com. All materials and context (texts, graphics, and every attributes) of CinemaAurora.com website are copyrights and trademarks of Cinema Aurora Any Commercia usage of materials and contents is forbidden without
                  prior permission from CinemaAurora. There is no other instituions/agencies outside CinemaAurora allowed to use www.cinemaAurora.com (Cinema Aurora Website) without prior permission from Cinema Aurora
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!--/.Second column-->
    </div>
  </div>
  <!--/.Footer Links-->
</footer>
<!--/.Footer-->

</html>


  @include('header')

        <!-- End of Navbar -->
        <style type="text/css">
          h3{
            color:black;
          }
          h4{
            color:black;
            font-size: 1em;
          }
        </style>
        <div class="row progressBarParent bs-wizard" style="border-bottom:0;">

            <div class="progressBar col-xs-4 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum"><h5>Shopping Cart</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step active"><!-- active -->
              <div class="text-center bs-wizard-stepnum"><h5>Payment</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>

            <div class="progressBar col-xs-4 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum"><h5>Order Confirmation</h5></div>
              <div class="progress"><div class="progress-bar"></div></div>
              <div class="bs-wizard-dot"></div>
            </div>
        </div>
        <?php
          $amount=0;

        ?>
        <table class="shoppingCartTable">
          <tr>
            <th>Title</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Sub-Total</th>
          </tr>
          @for($i=0; $i<count($cart->shoppingCart); $i++)
          <tr>
            <td class="cartContent">{{$cart->shoppingCart[$i]->movieId}}</td>
            <td class="cartContent">{{count($cart->shoppingCart[$i]->seats)}}</td>
            <td class="cartContent">{{$cart->shoppingCart[$i]->price}}</td>
            <td class="cartContent">{{ $amount+=((int)$cart->shoppingCart[$i]->price * count($cart->shoppingCart[$i]->seats)) }}</td>

          </tr>
          @endfor

          <table class="totalCartTable">
            <tr>
              <td class="totalContent"></td>
              <td class="totalContent"></td>
              <td class="TotalContent">Total</td>
              <td class="TotalContent">{{$amount}}</td>
            </tr>
          </table>
        </table>
        <div class="container paymentConfirmation" style="margin-top: 5%">
          <div class="col-md-12">
            <div class="jumbotron" style="background-color: #f1f1f1 ">
              <h3>Payment Confirmation</h3>
              <hr>
              <form method="POST" action="/checkout" accept-charset="UTF-8">
                <table>
                  <tr>
                    <td colspan="2">
                      <div class="form-group">
                        <label for="name">Full Name</label>
                          <input type="text" name="fullname" id="name" class="form-control" placeholder="Enter Full Name">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name ="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                        <small id="emailHelp" class="form-text text-muted">*We'll never share your email with anyone else.</small>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <div class="form-group">
                        <label for="CreditCardNo">Credit Card No.</label>
                          <input type="number" name="creditcard" id="CreditCardNo" placeholder="Enter Credit Card Number" class="form-control" required>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="cvv">CVV</label>
                          <input type="number" name="cvv" id="cvv" class="form-control" placeholder="Enter CVV" pattern="/^[0-9]{3,4}$/" oninvalid="setCustomValidity('Please Enter Valid cvv ')"
                          onchange="try{setCustomValidity('')}catch(e){}" required>
                      </div>
                    </td>
                    <td>
                      <div class="form-group">
                        <label for="expDate">Expiration Date</label>
                          <input type="text" name="expiration" id="expDate" class="form-control" placeholder="Enter Expiration Date" pattern="^(0[1-9]|1[0-2])\/?(([0-9]{4}|[0-9]{2})$)" oninvalid="setCustomValidity('Please Enter Valid Expiration Date ')"
                          onchange="try{setCustomValidity('')}catch(e){}" required>
                      </div>
                    </td>
                  </tr>
                </table>
                <div class="escapeCheckout">
                <button type="submit" class="btn btn-outline-danger">Back</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                {{ csrf_field() }}
              </div>
              </form>

            </div>
          </div>

        </div>
      @include('footer')

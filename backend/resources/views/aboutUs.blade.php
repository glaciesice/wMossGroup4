
  @include('header')

        <!-- End of Navbar -->

        <div class="aboutUs">
          <h2>About Us</h2>
          <hr/ style="width:40%">
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <p>
               &nbsp;&nbsp;&nbsp;Phasellus egestas felis sit amet lacus fringilla cursus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tellus nunc, placerat et convallis sit amet, consectetur a massa. Vivamus semper sollicitudin metus quis ultrices. Vivamus sit amet scelerisque ex. Curabitur porttitor hendrerit risus sed semper. Fusce placerat at velit vel suscipit. Nam laoreet lobortis rhoncus. Nam bibendum, odio aliquet consectetur aliquet, elit nunc gravida diam, non pulvinar diam justo vitae mi. Quisque consectetur venenatis odio, id bibendum nibh posuere a. Praesent molestie, massa a placerat placerat, magna nisl fermentum orci, in tristique nunc turpis vel tellus. Duis vehicula euismod orci, tincidunt ultrices dui tempus vitae. In hac habitasse platea dictumst. Donec a purus urna. Aenean lacinia neque eu erat tristique, in accumsan sem molestie.
              </p>
            </div>
          </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d278.8940651739699!2d14.636092824314414!3d38.06811221556358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1316eaa03cd2b7c7%3A0xc354e86ce36de606!2sCinema+Aurora!5e0!3m2!1sen!2sau!4v1525244929160" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="contactUs">
          <h2>Contact Us</h2>
          <hr/ style="width:60%">
          <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-2 contactIcon">
              <div class="circleThis">
                <i class="fas fa-envelope"></i>
              </div>
              <h4>EMAIL</h4>
              <a href="mailto:contact@cinemaaurora.com">contact@cinemaaurora.com</a>
            </div>
            <div class="col-sm-2 contactIcon">
              <div class="circleThis">
                <i class="fas fa-phone"></i>
              </div>
              <h4>CALL</h4>
              <a href="tel:+390941701042">+39 0941 701 042</a>
            </div>
            <div class="col-sm-2 contactIcon">
              <div class="circleThis">
                <i class="fab fa-twitter"></i>
              </div>
              <h4>TWITTER</h4>
              <a href="https://www.twitter.com/cinemaaurorasantagatadimilitello">@cinemaaurora</a>
            </div>
            <div class="col-sm-2 contactIcon">
              <div class="circleThis">
                <i class="fab fa-facebook-f"></i>
              </div>
              <h4>FACEBOOK</h4>
              <a href="https://www.facebook.com/cinemaaurorasantagatadimilitello">cinemaaurora</a>
            </div>
          </div>
        </div>
        
      @include('footer')

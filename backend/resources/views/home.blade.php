<?php
  use App\Http\Controllers\MovieController;
  $playingNowParam=["moviePlayingNow"=>1];
  $comingSoonParam=["moviePlayingNow"=>0];
  $movies=MovieController::allMovies();
  $playingNowList=MovieController::playingNowName($movies);
  $comingSoonList=MovieController::comingSoonName($movies);
  $playingNowDesc=MovieController::getDesc($movies,$playingNowParam);
  $comingSoonDesc=MovieController::getDesc($movies,$comingSoonParam);

?>
  @include('header')

  <!-- End of Navbar -->

  <div id="mainSliderCarousel" class="carousel slide" data-ride="carousel" style="width: 80%; margin-left: 10%" data-interval="4000">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <a href="{{url('/MovieDescription/MV0004/22-05-2018')}}"><img class="d-block w-100" src="{{ asset('image/Avengers Infinity War.jpg') }}" alt="First slide"></a>
      </div>
      <div class="carousel-item">
        <a href="{{url('/MovieDescription/MV0008/22-05-2018')}}"><img class="d-block w-100" src="{{ asset('image/Ralph Breaks the Internet Wreck-It Ralph 2.jpg') }}" alt="Second slide"></a>
      </div>
      <div class="carousel-item">
        <a href="{{url('/MovieDescription/MV0002/22-05-2018')}}"><img class="d-block w-100" src="{{ asset('image/Black Panther.jpg') }}" alt="Third slide"></a>
      </div>
    </div>
    <a class="carousel-control-prev" href="#mainSliderCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#mainSliderCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>



  <div class="container" style="height:50%">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-6">
          <h3 class="sub-title">Playing Now</h3>
        </div>
      </div>
      <hr style="width:90%">
        <div class="PlayingNowSlider" >
          <div class="container-fluid">
            <div id="playingNowCarousel" class="carousel slide" data-ride="carousel" data-interval="9000">
              <div class="carousel-inner row w-100 mx-auto" role="listbox">
                @for($i = 0; $i < count($playingNowList);++$i)
<?php $datetime = new DateTime();$url = '/MovieDescription/'.$playingNowList[$i]->movieId.'/'.$datetime->format('d-m-Y'); ?>
                <div class="carousel-item col-md-3 active">
                  <div class="card">
                    <img class="card-img-top" src="{{ asset('image/nowPlaying/'.$playingNowList[$i]->movieTitle.'.jpg') }}" alt="{{ $playingNowList[$i]->movieTitle }}">
                    <div class="card-body">
                      <h4 class="card-title">{{ $playingNowList[$i]->movieTitle }}</h4>
                      <a href="{{$url}}" class="btn btn-primary">Order Ticket</a>
                    </div>
                  </div>
                </div>
                @endfor
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>



<div class="container" style="height:50%">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-6">
        <h3 class="sub-title">Playing Now</h3>
      </div>
    </div>
  <hr style="width:90%">
  <div class="ComingSoonSlider">
    <div class="container-fluid">
      <div id="comingSoonCarousel" class="carousel slide" data-ride="carousel" data-interval="9000">
        <div class="carousel-inner row w-100 mx-auto" role="listbox">
          @for($i = 0; $i < count($comingSoonList);++$i)
          <div class="carousel-item col-md-3 active">
            <?php $datetime = new DateTime();$url = '/MovieDescription/'.$comingSoonList[$i]->movieId.'/'.$datetime->format('d-m-Y'); ?>
            <div class="card">
              <img class="card-img-top" src="{{ asset('image/nowPlaying/'.$comingSoonList[$i]->movieTitle.'.jpg') }}" alt="{{ $comingSoonList[$i]->movieTitle }}">
              <div class="card-body">
                <h4 class="card-title">{{ $comingSoonList[$i]->movieTitle }}</h4>
                <a href="{{$url}}" class="btn btn-primary">Read More</a>
              </div>
            </div>
          </div>
          @endfor
        </div>
      </div>
    </div>
  </div>
  <script>
    $('#playingNowCarousel').on('slide.bs.carousel', function(e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 4;
      var totalItems = $('.carousel-item').length;

      if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i = 0; i < it; i++) {
          // append slides to end
          if (e.direction == "left") {
            $('.carousel-item').eq(i).appendTo('.carousel-inner');
          } else {
            $('.carousel-item').eq(0).appendTo('.carousel-inner');
          }
        }
      }
    });
  </script>
  <script>
    $('#comingSoonCarousel').on('slide.bs.carousel', function(e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 4;
      var totalItems = $('.carousel-item').length;

      if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i = 0; i < it; i++) {
          // append slides to end
          if (e.direction == "left") {
            $('.carousel-item').eq(i).appendTo('.carousel-inner');
          } else {
            $('.carousel-item').eq(0).appendTo('.carousel-inner');
          }
        }
      }
    });
  </script>
@include('footer')


  @include('header')

  <?php
  $id = Route::input('id');
  // dd($id);
  $idN = $id + 1;
  $idP = $id - 1;
  $next = "/ComingSoon/id=".$idN;
  $prev = "/ComingSoon/id=".$idP;
  ?>

        <!-- End of Navbar -->


        <h1 class="title-page">Coming Soon</h1>
        <hr style="width:90%">

        <div class="container">
          <div class="col-md-12">
            <div class="jumbotron" style="background-color: white">
              <?php
                for($i = 0 ; $i <count($movies)-1; $i+=2){
              ?>
              <div class="container">

                <div class="row">
                  <div class="col-md-1">

                  </div>
                    <div class="col-md-4">
                      <div class="card">
                        <img class="card-img-top" src="{{ asset('image/nowPlaying/'.$movies[$i]->movieTitle.'.jpg') }}" alt={{$movies[$i]->movieTitle}}>
                        <div class="card-body">
                          <h4 class="card-title">{{$movies[$i]->movieTitle}}</h4>
                          <p class="card-text">
                            {{$movies[$i]->movieDescription}}
                          </p>
                          <?php
                            $datetime = new DateTime();
                            $url = '/MovieDescription/'.$movies[$i]->movieId.'/'.$datetime->format('d-m');
                          ?>
                          <a href="{{url($url)}}" class="btn btn-primary">Read More</a>
                        </div>
                        </div>
                    </div>
                  <div class="col-md-2">

                  </div>
                  <div class="col-md-4" href="#">
                    <div class="card">
                      <img class="card-img-top" src="{{ asset('image/nowPlaying/'.$movies[$i+1]->movieTitle.'.jpg') }}" alt={{$movies[$i+1]->movieTitle}}>
                        <div class="card-body">
                          <h4 class="card-title">{{$movies[$i+1]->movieTitle}}</h4>
                          <p class="card-text">
                            {{$movies[$i+1]->movieDescription}}
                          </p>
                          <?php
                            $datetime = new DateTime();
                            $url = '/MovieDescription/'.$movies[$i+1]->movieId.'/'.$datetime->format('d-m');
                          ?>
                          <a href="{{url($url)}}" class="btn btn-primary">Read More</a>
                      </div>
                      </div>
                  </div>
                </div>
              </div>
              <?php
                }
              ?>
                <nav aria-label="Page navigation example" >
                  <ul class="pagination justify-content-center">
                    <?php
                    if($id != 1){
                    ?>
                      <li class="page-item">
                        <a class="page-link" href={{ url($prev) }} tabindex="-1">Previous</a>
                      </li>
                    <?php
                      }
                    ?>
                    <li class="page-item"><a class="page-link" href="/ComingSoon/id=1">1</a></li>
                    <li class="page-item"><a class="page-link" href="/ComingSoon/id=2">2</a></li>
                    <?php
                    if($id < 2){
                    ?>
                      <li class="page-item">
                        <a class="page-link" href={{ url($next) }}>Next</a>
                      </li>
                    <?php
                      }
                    ?>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      @include('footer')

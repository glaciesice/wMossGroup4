

<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Cinema Aurora - Home</title>
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

  <!-- Fonts -->
</head>

<body>

  <!-- Navbar -->

  <nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="/"><img class="d-block w-100" src="{{ asset('image/cinemaAuroraLogo.png') }}" alt="Cinema Aurora"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background-color: black">
      <span class="navbar-toggler-icon" style="background-color: white"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="nav navbar-nav ml-auto" style="margin-right: 3em;">
        <li class="nav-item search-container">
          <form class="form-inline my-2 my-lg-0" action="search/" method="post" >
            {{ csrf_field() }}
            <input class="form-control mr-sm-1" name="name" type="search" placeholder="Search" aria-label="Search">
            <button class="btn my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
          </form>
        </li>
        <li class="nav-item active navbarCustom">
          <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item navbarCustom">
          <a class="nav-link" href="/PlayingNow/id=1">Playing Now</a>
        </li>
        <li class="nav-item navbarCustom">
          <a class="nav-link" href="/ComingSoon/id=1">Coming Soon</a>
        </li>
        <li class="nav-item navbarCustom">
          <a class="nav-link" href="/faq">F A Q</a>
        </li>
        <li class="nav-item navbarCustom">
          <a href="/cart">
            <img src="{{ asset('image/shoppingCart.png') }}" />
          </a>
        </li>
      </ul>
    </div>
  </nav>

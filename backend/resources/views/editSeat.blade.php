
  @include('header')
        <!-- End of Navbar -->

        <div class="selectSeat">
          <div class="container">
            <h2>Select seat for {{ $movieTitle }}</h2>
            <h3>on {{$dateString}} at {{$time}}</h3>
          </div>
        </div>

        <form method="POST" action="/editSeat" accept-charset="UTF-8">
          {{ csrf_field() }}
          <input type="hidden" name="movieId" value="{{$movieId}}" />
          <input type="hidden" name="time" value="{{$time}}" />
          <input type="hidden" name="date" value="{{$date}}" />
                <div class="container">
                  <div class="theatre">
                      <ol>
                        <li class="row row--1">
                          <ol class="seats" type="A">
                            @for($i = 1; $i <= 10; $i++ )
                            <li class="seat">
                              @if(in_array("A".$i, $customerSeats))
                                <input type="checkbox" id="{{"A" . $i}}" checked name="seats[{{"A" . $i}}]"/>
                              @elseif(in_array("A".$i, $otherSeats))
                                <input type="checkbox" disabled id="{{"A" . $i}}" />
                              @else
                                <input type="checkbox" id="{{"A" . $i}}" name="seats[{{"A" . $i}}]"/>
                              @endif
                              <label for="{{ "A" . $i }}">{{"A" . $i}}</label>
                            </li>
                            @endfor
                          </ol>
                        </li>
                        <li class="row row--2">
                          <ol class="seats" type="A">
                            @for($i = 1; $i <= 10; $i++ )
                            <li class="seat">
                              @if(in_array("B".$i, $customerSeats))
                                <input type="checkbox" id="{{"B" . $i}}" checked name="seats[{{"B" . $i}}]"/>
                              @elseif(in_array("B".$i, $otherSeats))
                                <input type="checkbox" disabled id="{{"B" . $i}}" />
                              @else
                                <input type="checkbox" id="{{"B" . $i}}" name="seats[{{"B" . $i}}]"/>
                              @endif
                              <label for="{{ "B" . $i }}">{{"B" . $i}}</label>
                            </li>
                            @endfor
                          </ol>
                        </li>
                        <li class="row row--3">
                          <ol class="seats" type="A">
                            @for($i = 1; $i <= 10; $i++ )
                            <li class="seat">
                              @if(in_array("C".$i, $customerSeats))
                                <input type="checkbox" id="{{"C" . $i}}" checked name="seats[{{"C" . $i}}]"/>
                              @elseif(in_array("C".$i, $otherSeats))
                                <input type="checkbox" disabled id="{{"C" . $i}}" />
                              @else
                                <input type="checkbox" id="{{"C" . $i}}" name="seats[{{"C" . $i}}]"/>
                              @endif
                              <label for="{{ "C" . $i }}">{{"C" . $i}}</label>
                            </li>
                            @endfor
                          </ol>
                        </li>
                        <li class="row row--4">
                          <ol class="seats" type="A">
                            @for($i = 1; $i <= 10; $i++ )
                            <li class="seat">
                              @if(in_array("D".$i, $customerSeats))
                                <input type="checkbox" id="{{"D" . $i}}" checked name="seats[{{"D" . $i}}]"/>
                              @elseif(in_array("D".$i, $otherSeats))
                                <input type="checkbox" disabled id="{{"A" . $i}}" />
                              @else
                                <input type="checkbox" id="{{"D" . $i}}" name="seats[{{"D" . $i}}]"/>
                              @endif
                              <label for="{{ "D" . $i }}">{{"D" . $i}}</label>
                            </li>
                            @endfor
                          </ol>
                        </li>
                        <li class="row row--5">
                          <ol class="seats" type="A">
                            @for($i = 1; $i <= 10; $i++ )
                            <li class="seat">
                              @if(in_array("E".$i, $customerSeats))
                                <input type="checkbox" id="{{"E" . $i}}" checked name="seats[{{"E" . $i}}]"/>
                              @elseif(in_array("E".$i, $otherSeats))
                                <input type="checkbox" disabled id="{{"E" . $i}}" />
                              @else
                                <input type="checkbox" id="{{"E" . $i}}" name="seats[{{"E" . $i}}]"/>
                              @endif
                              <label for="{{ "E" . $i }}">{{"E" . $i}}</label>
                            </li>
                            @endfor
                          </ol>
                        </li>
                    </ol>
                  </div>
                  <h4 class="screenTitle">Screen</h3>
                </div>


                <div class="exitButtonsSeat">
                  <button type="button" class="btn btn-outline-danger">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
        </form>

        @include('footer')
